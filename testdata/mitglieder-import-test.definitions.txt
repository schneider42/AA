Erläuterung der Felder:

AKT,L
Aktiv, boolean

ORG,C,30
Organisation (Teil der Anschrift), 30 Zeichen

VNM,C,10
Vorname, 10 Zeichen

NME,C,20
Nachname

STR,C,30
Straße

LND,C,3
Land

PLZ,N,5,0
Postleitzahl, 5-stellige Zahl, per Default 0

ORT,C,40

BND,C,30
Bundesland, eigentlich ungenutzt, steht aber in Einträgen <= 2043 oder
so was drin.

EML,C,50
Email, öfters durch Komma getrennt mehrere Einträge

RST,N,3,0
Rest. Wie viele Datenschleudern noch zu senden sind. Bei Abonnenten
steht hier etwas wie z.B. 106, wenn sie zum Zeitpunkt, also die DS 94
draußen war 12 Stück geordert haben. Bei (Förder)Mitgliedern steht hier 999.

TEL,C,15;FAX,C,15;BLZ,C,8;BNK,C,20;KTO,C,12;
Werden nicht mehr benutzt.

EZE,L
Einzugsermächtigung. Missbrauchen wir derzeit als Flag für
Fördermitglieder. True == Fördermitglied, False == Mitglied.

UBK,L
Unbekannt verzogen. True == Irgendwann ist mal ein Brief/Datenschleuder
unzustellbar gewesen.

MTG,L
Mitglied. False == Datenschleuder-Abonnent

ERM,L
Ermäßigt?

DAU,L
Dauerauftrag eingerichtet? True == ist Doppelmitglied, das vom Erfa
verwaltet wird.

PRV,L
Pressevertreter, DS-Abonnement, der nichts zahlen muss, da von der Presse

AAA,L
Austausch-Abonnement. Weiß ich gar nicht genau was dahinter steckt.
Haben wir 28 mal in der Datenbank und bedeutet vermutlich auch, dass es
kostenfrei als Austausch für eine andere Publikation gesendet wird.

FRE,L
Freiabo, DS-Abonnement, der nichts zahlen muss, da irgendwie besonders


LZA,D
Letzte Zahlung am, Datum

LUP,D
Datensatz zuletzt aktualisiert am

EIN,D
Eintrittsdatum

BZB,D
Bezahlt bis

AAM,D
Ausgetreten am

KOM,C,60
Kommentar. Freitext-Feld, das wir derzeit für vielerlei Dinge brauchen
wie z.B. Nachrichten an das Mitglied (Bei Überweisungen immer
Chaosnummer angeben!) oder interne Anweisungen (Dublette von
$Chaosnummer, Austritt per Email vom $Datum)

ERK,C,3
Erfakreis-Kürzel.

DTF,L
TOUR,N,2,0
DEPOT,N,1,0
CD,N,1,0
Diese Felder werden allesamt schon lange nicht mehr genutzt und können
ignoriert werden.


NR,N,5,0
Chaosnummer

PGPKEY,C,42

