# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0009_member_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='account_balance',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_last_paid',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2017, 4, 4, 20, 25, 10, 217201)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='member',
            name='fee_paid_until',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2017, 4, 4, 20, 25, 19, 952065)),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='member',
            name='membership_type',
            field=models.CharField(choices=[('SUP', 'SUPPORTER'), ('MBR', 'MEMBER'), ('HON', 'HONORARY')], max_length=3, default='SUP'),
        ),
    ]
