from django.contrib import admin
from django import forms
from ROOT.settings import DEBUG

from .models import (
    ArchivedEmail, BalanceTransactionLog, EmailAddress,
    EmailToMember, Erfa, Member,
)


@admin.register(EmailAddress)
class EmailAdmin(admin.ModelAdmin):
    readonly_fields = ('gpg_error',)


class EmailInline(admin.StackedInline):
    readonly_fields = ('gpg_error',)
    extra = 0
    model = EmailAddress

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.attname == 'gpg_key_id':
            kwargs['widget'] = forms.TextInput(
                attrs={'oninput': "this.value = this.value.replace(/\s+/g, '');", 'size': 40})
        return super(EmailInline, self).formfield_for_dbfield(db_field, **kwargs)


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    inlines = [
        EmailInline,
    ]

    list_display = ('chaos_number', 'get_name', 'get_address', 'is_member', 'is_active', 'fee_paid_until',
                    'account_balance', 'get_emails_string', 'comment')
    search_fields = ('chaos_number', 'first_name', 'last_name', 'address_1', 'address_2', 'address_3',
                     'emailaddress__email_address', '=address_country', 'comment')

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ["chaos_number",
                                "membership_end",
                                "fee_last_paid",
                                "fee_paid_until"
                                ]

        return super().change_view(request, object_id, form_url, extra_context)

    def add_view(self, request, form_url='', extra_context=None):
        self.exclude = ["chaos_number",
                        "membership_end",
                        "fee_last_paid",
                        "fee_paid_until",
                        "fee_registration_paid"]

        return super().add_view(request, form_url, extra_context)

    def save_model(self, request, obj, form, change):
        if 'account_balance' in form.changed_data:
            increase_by = obj.account_balance - obj._initial_state['account_balance']
            obj.log_increased_balance(increased_by=increase_by,
                                      reason=BalanceTransactionLog.MANUAL_BOOKING,
                                      comment="balance set via member admin")
        super(MemberAdmin, self).save_model(request, obj, form, change)


@admin.register(Erfa)
class ErfaAdmin(admin.ModelAdmin):
    pass


@admin.register(EmailToMember)
class EmailToMemberAdmin(admin.ModelAdmin):
    # list_display = ('member', 'subject', 'body', 'created', 'ready_to_send', 'email_type', ')

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.readonly_fields = ['rendered_preview']
        return super().change_view(request, object_id, form_url, extra_context)


@admin.register(ArchivedEmail)
class EmailArchiveAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(BalanceTransactionLog)
class BalanceTransactionLogAdmin(admin.ModelAdmin):
    readonly_fields = ('created_on', 'member', 'changed_value', 'new_value')
