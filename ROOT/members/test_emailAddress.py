from django.test import TestCase
from unittest.mock import create_autospec, patch, Mock, ANY
import members.models
import gnupg
from ddt import ddt, data, unpack


@ddt
class TestEmailAddress(TestCase):

    def setUp(self):
        # One member is all we need
        self.member = members.models.Member(first_name='first', last_name='last', address_1='address',
                                            address_country='DE')
        self.member.save()

    def test_uniqueness(self):
        from django.db import IntegrityError
        email_address_1 = members.models.EmailAddress(member=self.member, email_address='a@example.com')
        email_address_2 = members.models.EmailAddress(member=self.member, email_address='a@example.com')
        email_address_1.save()
        with self.assertRaises(IntegrityError, msg='If this fails try deleting your db.sqlite3 and rebuilding it.'):
            email_address_2.save()

    @data(
        [[{'email_address': 'a@example.com'}], [True]],
        [[
            {'email_address': 'a@example.com'},
            {'email_address': 'b@example.com', 'gpg_key_id': '0x12341234'}
        ], [True, False]],
        [[
            {'email_address': 'a@example.com'},
            {'email_address': 'b@example.com', 'is_primary': True}
        ], [False, True]]
    )
    @unpack
    def test_primary_at_creation(self, email_addresses, primary_states):
        for email_address in email_addresses:
            members.models.EmailAddress(member=self.member, **email_address).save()

            self.assertEqual(members.models.EmailAddress.objects.filter(is_primary=True).count(), 1,
                             'There should be exactly one primary email address at any given time')

        self.assertEquals([email.is_primary for email in members.models.EmailAddress.objects.all()], primary_states)

    def test_primary_at_deletion(self):
        email_address_1 = members.models.EmailAddress(member=self.member, email_address='a@example.com')
        email_address_1.save()
        email_address_2 = members.models.EmailAddress(
            member=self.member, email_address='b@example.com', is_primary=True)
        email_address_2.save()
        email_address_2.delete()

        self.assertTrue(email_address_1.is_primary)
