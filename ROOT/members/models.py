# -*- coding: utf-8 -*-
import datetime
import logging
import re
from collections import defaultdict

import gpg
import pendulum
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.template import Context, loader
from members.countryfield import CountryField


class ChangedFieldsBase(models.Model):
    """Saves the state of an object at initialization time and provides the method have_fields_changed(list) to check
    if a certain group of fields has been modified."""
    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initial_state = self._state_as_dict()

    def _state_as_dict(self):
        """
        Returns all fields in their current state, but not relation fields like foreign key fields.

        :return: Dictionary of field names to field values.
        """
        return {
            f.name: getattr(self, f.name)
            for f in self._meta.local_fields
            if not f.rel
        }

    def save(self, *args, **kwargs):
        # Changed from checking modified values from database load time to init and save.
        # We do this because imagine the following case. A member is loaded from database, edited and saved. An email
        # is created and then sent via the api. All the time, the member object is still in memory and then changed
        # again, but this time not fetched from the database. If the changes, by chance, are back to the previous values
        # those changes would not create another email. This could for example happen when a member is quickly changing
        # his or her mind about a change. Typical: Please add my Dr. title to my name. On second thoughts, don't.
        super().save(*args, **kwargs)
        self._initial_state = self._state_as_dict()

    def have_fields_changed(self, fields):
        """
        Check if any field of interest has been modified.

        :param fields: List of field names to check for if any of them has been modified
        :return: True if any field in 'fields' has been modified, False otherwise
        """
        return any(key in fields for key, value in self._initial_state.items() if value != self._state_as_dict()[key])


class MemberManager(models.Manager):

    def members_only(self):
        # ATTENTION: if you change this logic, you have to change the corresponding logic in Member.is_member
        return self.get_queryset().filter(
            Q(membership_end__isnull=True) | Q(membership_end__gt=datetime.datetime.now())
        )


class Erfa(models.Model):
    # We use shortname in part as filename, e.g. we don't want any spaces and or dots.
    short_name = models.SlugField(blank=False, null=False, unique=True, max_length=255)
    long_name = models.CharField(blank=False, null=False, unique=True, max_length=255)
    has_doppelmitgliedschaft = models.BooleanField(default=False)

    def __str__(self):
        desc = self.short_name
        if len(self.long_name) > 0:
            desc += ' (' + self.long_name + ')'
        if self.has_doppelmitgliedschaft:
            desc += ' Doppelmitgliedschaft'
        return desc


def get_alien():
    return Erfa.objects.get_or_create(short_name='Alien')[0].id


class Member(ChangedFieldsBase):
    """
    This model represents a person, that is or was a member of the association.
    """

    MEMBERSHIP_TYPE_SUPPORTER = 'SUP'
    MEMBERSHIP_TYPE_MEMBER = 'MBR'
    MEMBERSHIP_TYPE_HONORARY = 'HON'

    MEMBERSHIP_TYPE_CHOICES = (
        (MEMBERSHIP_TYPE_SUPPORTER, 'SUPPORTER'),
        (MEMBERSHIP_TYPE_MEMBER, 'MEMBER'),
        (MEMBERSHIP_TYPE_HONORARY, 'HONORARY'),
    )

    chaos_number = models.AutoField(unique=True, primary_key=True)
    membership_type = models.CharField(
        max_length=3, choices=MEMBERSHIP_TYPE_CHOICES, default=MEMBERSHIP_TYPE_SUPPORTER
    )

    is_active = models.BooleanField(default=True)

    membership_reduced = models.BooleanField(default=False)
    membership_reduced.short_description = 'ermäßigte mitgliedschaft / mitgliedschaft'

    erfa = models.ForeignKey(Erfa, blank=False, null=False, default=get_alien)
    first_name = models.CharField(blank=False, null=False, max_length=255)
    last_name = models.CharField(blank=False, null=False, max_length=255)

    # code blaming: address 1-3 reducing flexibility unnecessarily; text field w/ custom widget
    address_1 = models.CharField(blank=False, null=False, max_length=255)
    address_2 = models.CharField(blank=True, null=True, max_length=255)
    address_3 = models.CharField(blank=True, null=True, max_length=255)
    address_country = CountryField()

    address_unknown = models.BooleanField(default=False)
    address_unknown.short_description = 'will be set to FALSE on next address change'

    # only admin should change it
    membership_start = models.DateField(blank=False, null=False, default=datetime.date.today)

    # invis
    membership_end = models.DateField(blank=True, null=True)
    fee_last_paid = models.DateField(blank=False, null=False, default=datetime.date.today)
    fee_paid_until = models.DateField(blank=False, null=False, default=datetime.date.today)
    last_update = models.DateField(auto_now=True)
    account_balance = models.IntegerField(blank=False, null=False, default=0)
    account_balance.short_description = 'value is in cent'
    fee_override = models.IntegerField(blank=True, null=True, default=None)
    fee_override.short_description = 'Jährlicher, individueller Mitgliedsbeitrag, Überschreibt ALLE anderen Werte!'

    fee_registration_paid = models.BooleanField(default=False)

    comment = models.CharField(blank=True, null=False, max_length=255)

    # other non-var stuff
    objects = MemberManager()

    def __str__(self):
        if not self.is_member():
            membership = 'exited'
        elif not self.is_active:
            membership = 'inactive'
        else:
            membership = 'active'

        return 'Member {} {}'.format(self.chaos_number, membership)

    def is_member(self):
        # ATTENTION: if you change this logic, you have to change the corresponding logic in MemberManager.members_only
        return (self.membership_end is None) or (datetime.date.today() < self.membership_end)

    is_member.short_description = 'is member'

    def get_balance_readable(self):
        return '{:0.2f}'.format(float(self.account_balance) / 100.0)

    def get_years_payment_behind(self):
        if self.fee_paid_until < datetime.date.today():
            # +1 year because the result of .in_year() is rounded down
            return pendulum.instance(datetime.datetime(self.fee_paid_until)).diff(pendulum.now()).in_years() + 1
        else:
            return 0

    def get_money_to_pay(self):
        return (self.get_years_payment_behind() * self.get_annual_fee()) - self.account_balance

    def get_money_to_pay_readable(self):
        return '{:0.2f}'.format(float(self.get_money_to_pay()) / 100.0)

    def get_name(self):
        return '{} {}'.format(self.first_name, self.last_name)
    get_name.short_description = "Full name"
    get_name.admin_order_field = 'first_name'

    def get_plz(self):
        if self.address_country == 'DE':
            regex_result = re.search(r'[0-9]{4,5}', self.get_address())
            if regex_result is not None:
                return regex_result.group()

        return ''

    def get_address(self, separator=' '):
        if self.address_unknown:
            return 'unknown'

        parts = [self.address_1, self.address_2, self.address_3]
        return separator.join(p for p in parts if p is not None)
    get_address.short_description = 'Address'
    get_address.admin_order_field = 'address_1'

    def set_address_unknown(self):
        self.address_unknown = True
        self.save(update_fields=['address_unknown'])

    def get_emails(self):
        return self.emailaddress_set.all()

    def get_emails_string(self):
        return ', '.join(str(x) for x in self.emailaddress_set.all())
    get_emails_string.short_description = 'E-Mail(s)'
    get_emails_string.admin_order_field = 'email'

    def get_annual_fee(self):
        if self.fee_override is not None:
            return self.fee_override
        elif self.membership_reduced:
            return settings.FEE_REDUCED
        return settings.FEE

    def get_annual_fee_readable(self):
        return '{:0.2f}'.format(self.get_annual_fee() / 100)

    def email_is_unknown(self):
        return not self.emailaddress_set.all().exist()

    def get_primary_mail(self):
        primary_mail = self.emailaddress_set.filter(is_primary=True).first()
        if primary_mail:
            return primary_mail
        general_mail = self.emailaddress_set.first()
        if general_mail:
            general_mail.is_primary = True
            general_mail.save()
        return general_mail

    def alienate(self, erfa):
        if self.erfa != erfa:
            return False
        erfa, created = Erfa.objects.get_or_create(short_name=settings.EMPTY_ERFA_NAME)
        if created:
            erfa.long_name = settings.EMPTY_ERFA_NAME
            # TODO: do we need this? please test if child objects are automatically saved
            erfa.safe()
        self.erfa = erfa

    def exit(self):
        exit_mail = EmailToMember(email_type=EmailToMember.SEND_TYPE_EXIT, member=self)
        exit_mail.save()
        exit_mail.send()

        # everything except TODAY makes problems with "members_only" and "is_member"
        self.membership_end = datetime.date.today()
        self.membership_reduced = False
        self.is_active = False
        self.erfa = Erfa.objects.get_or_create(short_name='Alien')[0]
        self.first_name = ''
        self.last_name = ''
        self.address_1 = ''
        self.address_2 = None
        self.address_3 = None
        self.address_country = ''
        self.fee_last_paid = datetime.date.today()
        self.fee_paid_until = datetime.date.today()
        self.fee_override = None
        self.last_update = datetime.date.today()
        self.comment = ''
        self.emailaddress_set.all().delete()
        self.emailtomember_set.all().delete()
        self.save()

    def set_balance_to(self, new_balance, reason=None, comment='', booking_day=None, save=True):
        # calculates transaction and passes to increaseBalanceBy
        increase_by = new_balance - self.account_balance
        self.increase_balance_by(increase_by, reason, comment, booking_day, save)

    def increase_balance_by(self, increase_by, reason=None, comment='', booking_day=None, save=True):
        # all booked balance changes should occur via this function
        self.account_balance += increase_by
        if booking_day:
            self.fee_last_paid = booking_day
        logging.info('increasing balance of member: {} by: {}'.format(self.chaos_number, increase_by))
        self.log_increased_balance(increase_by, reason, comment, booking_day)
        if save:
            self.save()

    def log_increased_balance(self, increased_by, reason=None, comment='', booking_day=None):
        transaction_log = BalanceTransactionLog()
        logging.info('balance used to be {}'.format(self._initial_state['account_balance']))
        transaction_log.save_log(member=self, amount=increased_by, new_value=self.account_balance,
                                 comment=comment, reason=reason, booking_day=booking_day)

    def execute_payment_if_due(self, save=True):
        """
        Checks if the next annual fee is due and processes it, if enough money is in the account_balance.
        :return: True, if a payment has been processed, False otherwise.
        """
        if self.membership_type == self.MEMBERSHIP_TYPE_HONORARY or not self.is_active:
            return False

        # Do not run on the same date because that would cause to run it the day a new member was entered and we
        # know for sure no money arrived yet (except in the Vereinstisch case).
        # Everything is okay, go ahead and "transform" money to membership years
        if self.fee_paid_until < datetime.date.today():
            years = relativedelta(datetime.date.today(), self.fee_paid_until).years + 1
            # keep this order because increase_balance_by calls save()
            self.fee_paid_until += relativedelta(years=years)
            self.increase_balance_by(-self.get_annual_fee()*years,
                                     reason=BalanceTransactionLog.BILLING_CYCLE, save=save)
            return True
        else:
            return False

    def save(self, *args, **kwargs):
        # If the address was formerly unknown and now any part of it has been changed, but the address_unknown field has
        # not been unchecked, then assume it has been forgotten and uncheck that field automatically.
        if self._initial_state['address_unknown'] and not self.have_fields_changed(['address_unknown']) \
                and self.have_fields_changed(['address_1', 'address_2', 'address_3', 'address_country']):
            self.address_unknown = False

        # On reactivation of a member their account balance and payment due date should be set as if they were a new
        # member
        if not self._initial_state['is_active'] and self.have_fields_changed(['is_active']):
            self.set_balance_to(0, save=False, comment='Outstanding fees waived after reactivation.')
            self.fee_paid_until = datetime.date.today()

        # Check if fee payment is due and adjust balance and due date accordingly
        self.execute_payment_if_due(save=False)
        super().save(*args, **kwargs)

    def clean(self):
        # If an Erfa with Doppelmitgliedschaft is chosen, but as membership type a supporter is selected raise an error
        if self.membership_type == self.MEMBERSHIP_TYPE_SUPPORTER and self.erfa.has_doppelmitgliedschaft:
            raise ValidationError("Only Members are allowed as Doppelmitglieder")

    def _fix_primary_mail(self):
        # make sure we have only one primary email!
        primary_emails = list(self.emailaddress_set.filter(is_primary=True).all())
        for email in primary_emails[1:]:
            logging.warning("Member {} had more than one primary email. We disabled {}".format(self.pk, str(email)))
            email.is_primary = False
            email.save()
        if len(primary_emails) == 0:
            email = self.emailaddress_set.first()
            if email:
                logging.warning("Member {} has at least one email, but no primary. We set this email {} to"
                                "primary".format(self.pk, str(email)))
                email.is_primary = True
                email.save()


class EmailAddress(ChangedFieldsBase):
    class Meta:
        verbose_name = 'email address'
        verbose_name_plural = 'email addresses'
        unique_together = (('member', 'email_address'),)

    member = models.ForeignKey('Member')  # code blaming e.g. ('', related_name='emails')
    email_address = models.EmailField(blank=False, null=False, unique=False)
    is_primary = models.BooleanField(default=False)
    gpg_key_id = models.CharField(max_length=60, blank=True, null=False,
                                  validators=[RegexValidator(
                                      regex='^0x[a-fA-F0-9]{8}$|^0x[a-fA-F0-9]{16}$|^[a-fA-F0-9]{40}$',
                                      message='Enter a valid short or long ID or Fingerprint')]
                                  )

    gpg_error = models.CharField(max_length=256, blank=True, null=False)
    read_only = (gpg_error,)

    def save(self, *args, **kwargs):
        # Make sure there is always exactly one primary email address. Either this one is going to be it, then set this
        # attribute to false on all other EmailAddress objects, or make this one primary, if otherwise none would be.
        if self.is_primary:
            addresses = EmailAddress.objects.filter(member=self.member)
            if self.pk:
                addresses = addresses.exclude(pk=self.pk)
            addresses.update(is_primary=False)
        elif not EmailAddress.objects.filter(member=self.member, is_primary=True).exists():
            self.is_primary = True

        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)

        member_email_addresses = EmailAddress.objects.filter(member=self.member)
        if member_email_addresses.exists():
            if not member_email_addresses.filter(is_primary=True).exists():
                new_primary_email_address = member_email_addresses.order_by('-gpg_key_id').first()
                new_primary_email_address.is_primary = True
                new_primary_email_address.save()

    def __str__(self):
        if self.gpg_key_id:
            return '{} ({})'.format(self.email_address, self.gpg_key_id)
        else:
            return self.email_address


class TemplateError(Exception):
    pass


class EmailToMember(models.Model):
    class Meta:
        verbose_name_plural = "emails to send"

    SEND_TYPE_DEFAULT = 'def'
    SEND_TYPE_DATA_RECORD = 'dml'
    SEND_TYPE_WELCOME = 'wlc'
    SEND_TYPE_DELAYED_PAYMENT = 'dep'
    SEND_TYPE_GPG_ERROR = 'gpg'
    SEND_TYPE_EXIT = 'ext'
    EMAIL_TO_SEND_TYPES = (
        (SEND_TYPE_DEFAULT, 'default'),
        (SEND_TYPE_DATA_RECORD, 'data record'),
        (SEND_TYPE_WELCOME, 'welcome'),
        (SEND_TYPE_DELAYED_PAYMENT, 'delayed payment'),
        (SEND_TYPE_GPG_ERROR, 'gpg error'),
        (SEND_TYPE_EXIT, 'member exit')
    )

    member = models.ForeignKey('Member')
    subject = models.CharField(max_length=255, null=False, blank=True)
    body = models.TextField(null=False, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    email_type = models.CharField(max_length=3, choices=EMAIL_TO_SEND_TYPES, default=SEND_TYPE_DEFAULT)

    @staticmethod
    def _render_template(template_name, country_code, context):
        """
        Selects a template and renders it's subject and body sections.

        :param template_name: The part of the mail template file name between 'mail' and the country code.
        :param country_code: Two letter country code. Is appended to the template file name with an underline. EN is
        fallback if none is found.
        :param context: Depends on the objects used in the template.
        :return: subject and body of the email as a tuple of strings.
        """

        template_paths = ['mail_templates/mail_{}_{}.html'.format(template_name, country)
                          for country in [country_code, 'EN']]

        nodes = dict((n.name, n) for n in loader.select_template(template_paths).template.nodelist
                     if n.__class__.__name__ == 'BlockNode')

        try:
            return nodes['subject'].render(context), nodes['body'].render(context)
        except KeyError as ke:
            raise TemplateError('Template is missing a {% block {} %}'.format(ke))

    def rendered_preview(self):
        """
        A preview with the template's HTML rendered. Suitable for displaying the email in a browser.
        :return: Subject and body enclosed in <pre>-tags. String is safely encoded.
        """
        from django.utils import safestring
        return safestring.mark_safe('<pre>{}</pre><pre>{}</pre>'.format(self.subject, self.body))

    def render_subject_and_body(self):
        (self.subject, self.body) = self._render_template(
            self.get_email_type_display().replace(' ', '_'),
            self.member.address_country,
            Context({'member': self.member})
        )

    def save(self, *args, **kwargs):
        if not self.pk:  # object is being created, thus no primary key field yet
            self.render_subject_and_body()
        super(EmailToMember, self).save(*args, **kwargs)

    def send(self):
        def _send_mail(address, subject, body, should_archive=True):
            send_email = EmailMessage(
                subject=subject,
                body=body,
                from_email=settings.EMAIL_HOST_USER,
                to=address,
                bcc=[settings.EMAIL_HOST_USER, ]
            )
            try:
                send_state = send_email.send(False)
            except ConnectionRefusedError as ex:
                logging.error('Sending mail to {} failed with: {}'.format(address, ex))
                return {'mailTo': address, 'send_state': 0}
            if send_state > 0 and should_archive:
                self._archive(address)
            return {'mailTo': address, 'send_state': send_state}

        # First try the primary email address. Then try the addresses with a gpg key, if there are any. Last try
        # sending unencrypted
        email_addresses = self.member.get_emails().order_by('-is_primary', '-gpg_key_id')

        # If any email address has a gpg key id we will only send encrypted emails (or gpg error notifications)
        prefers_encrypted_email = self.member.get_emails().exclude(gpg_key_id='').exists()

        logging.info('Sending email for {}'.format(self.member.get_name()))
        with gpg.Context(armor=True) as c:
            # Use local keyring
            c.set_engine_info(gpg.constants.PROTOCOL_OpenPGP, home_dir=settings.GPG_HOME)

            # Get signing key from local keychain
            try:
                sender_key = c.get_key(settings.GPG_HOST_USER)
            except gpg.errors.KeyNotFound as ex:
                logging.error('Could not find signature key: {}'.format(ex))
                return {"mailTo": "", "send_state": 0}
            except gpg.errors.GPGMEError as ex:
                logging.error('GPG error with signature key: {}'.format(ex))
                return {"mailTo": "", "send_state": 0}
            c.signers = [sender_key]

            # Get recipient keys from keyservers
            c.set_keylist_mode(gpg.constants.keylist.mode.EXTERN)

            for email_address in email_addresses:
                logging.info(' Trying email address {}'.format(email_address))
                if email_address.gpg_key_id is not '':
                    logging.info(' Using gpg key {}'.format(email_address.gpg_key_id))
                    # try sending gpg email
                    try:
                        recipient_key = c.get_key(email_address.gpg_key_id)
                        c.op_import_keys([recipient_key])  # Important: If not imported to the context it's not usable
                        if email_address.gpg_key_id.startswith('0x') and recipient_key.fpr:
                            email_address.gpg_key_id = recipient_key.fpr
                            email_address.save()
                        encrypted_body, result, sign_result = c.encrypt(
                            self.body.encode(),
                            recipients=[sender_key, recipient_key],
                            sign=True, always_trust=True)
                        logging.info(' Encryption result: {}'.format(result))
                        logging.info(' Signature result: {}'.format(sign_result))
                        email_address.gpg_error = ''
                        return _send_mail([email_address.email_address], self.subject, encrypted_body.decode('utf-8'))
                    except gpg.errors.InvalidRecipients as ex:
                        # raised by encrypt()
                        email_address.gpg_error = 'Invalid Recipients: {}'.format(ex)
                        email_address.save()
                    except gpg.errors.InvalidSigners as ex:
                        # raised by encrypt()
                        email_address.gpg_error = 'Invalid Signers: {}'.format(ex)
                        email_address.save()
                    except gpg.errors.KeyNotFound as ex:
                        # raised by get_key()
                        email_address.gpg_error = 'Key not found on keyservers: {}'.format(ex)
                        email_address.save()
                    except gpg.errors.GPGMEError as ex:
                        # raised by get_key() and encrypt()
                        email_address.gpg_error = 'GPG Error: {}'.format(ex)
                        email_address.save()

                    logging.warning(' Sending GPG error message email: {}'.format(email_address.gpg_error))

                    # GPG error notifications will always be sent unencrypted
                    (error_subject, error_body) = self._render_template(
                        dict(self.EMAIL_TO_SEND_TYPES)[self.SEND_TYPE_GPG_ERROR].replace(' ', '_'),
                        self.member.address_country,
                        Context({'member': self.member, 'gpg_error': email_address.gpg_error,
                                 'email_address': email_address})
                    )
                    signed_body, sign_result = c.sign(error_body.encode(), mode=gpg.constants.SIG_MODE_CLEAR)
                    logging.info(' Signature result of GPG error message email: {}'.format(sign_result))
                    _send_mail([email_address.email_address], error_subject, signed_body.decode('utf-8'), False)
                    continue
                elif prefers_encrypted_email:
                    logging.warning(
                        ' Failed on all encryptable email addresses for member {}'.format(self.member.get_name()))
                    break
                else:
                    logging.info(' Sending unencrypted email to {}'.format(email_address))
                    signed_body, result = c.sign(self.body.encode(), mode=gpg.constants.SIG_MODE_CLEAR)
                    return _send_mail([email_address.email_address], self.subject, signed_body.decode('utf-8'))

        # No working email addresses were found
        logging.warning(' No working email addresses were found.')
        return {"mailTo": [], "send_state": 0}

    def _archive(self, email_address):
        mail_archive = ArchivedEmail()
        mail_archive.created_date = self.created
        mail_archive.sent_date = datetime.datetime.now()
        mail_archive.body = self.body
        mail_archive.subject = self.subject
        mail_archive.email_address = str(email_address)
        mail_archive.save()
        self.delete()

    def __str__(self):
        return ", ".join([self.subject, str(self.member)])


class ArchivedEmail(models.Model):
    email_address = models.CharField(max_length=255, null=False, blank=True)
    subject = models.CharField(max_length=255, null=False, blank=True)
    body = models.TextField(null=False, blank=True)
    # code blaming: time stamp model inheritance
    created_date = models.DateTimeField(null=False)
    sent_date = models.DateTimeField(auto_now=True)


class BalanceTransactionLog(models.Model):

    # note currently executing and not just logging, could maybe refactor so actual transaction execution
    # was part of Member and then call this as an actual log
    IMPORT_INITIAL = 'SYN'
    IMPORT_BANKING = 'BAI'
    BILLING_CYCLE = 'BIC'
    MANUAL_BOOKING = 'MAN'
    ACCOUNT_CLOSED = 'ACK'
    NO_INFO = 'NOI'

    member = models.ForeignKey('Member')
    changed_value = models.IntegerField(blank=False, null=False)
    new_value = models.IntegerField(blank=False, null=False)
    TRANSACTION_REASON_CHOICES = (
        (MANUAL_BOOKING, 'MANUAL BOOKING'),
        (IMPORT_INITIAL, 'INITIAL IMPORT'),
        (IMPORT_BANKING, 'BANKING IMPORT'),
        (BILLING_CYCLE, 'BILLING CYCLE'),
        (ACCOUNT_CLOSED, 'ACCOUNT CLOSED'),
        (NO_INFO, 'NO INFORMATION'),
    )
    transaction_reason = models.CharField(max_length=3, choices=TRANSACTION_REASON_CHOICES)
    comment = models.CharField(blank=True, null=True, max_length=255)
    created_on = models.DateTimeField(auto_now_add=True)
    booking_day = models.DateField(blank=False, null=False, default=datetime.date.today)

    def __str__(self):
        return "{}, Amount {}, Balance {}, {}, {}, Timestamp: {}".format(self.member, self.changed_value,
                                                                         self.new_value, self.transaction_reason,
                                                                         self.comment, self.created_on)

    def save_log(self, member, amount, new_value, reason=None, comment='', booking_day=None):

        if reason is None:
            reason = self.NO_INFO
        self.member = member
        self.comment = comment
        self.transaction_reason = reason
        self.changed_value = amount
        self.new_value = new_value
        if booking_day:
            self.booking_day = booking_day
        self.save()

    def save(self, *args, **kwargs):
        member = self.member
        defacto_increased_by = member.account_balance - member._initial_state['account_balance']
        # make sure for new logs that they match changes in active member objects
        if self.pk is None and round(self.changed_value, 2) != round(defacto_increased_by, 2):
            raise LoggingConsistencyError("balance change ({}) does not match ".format(self.changed_value) +
                                          "actual change in balance ({}) since loading".format(defacto_increased_by))
        super().save()


class LoggingConsistencyError(Exception):
    pass


def send_or_refresh_data_record(member):
    """
    This will queue an data record email for the specified Member or refresh any already queued data record email.
    :param member: An instance of a Member object, of whom the data record will be sent.
    :return: None
    """
    # This should only ever yield zero or one emails. But just in case we handle it like it could be any number.
    emails_in_queue = EmailToMember.objects.filter(
        Q(email_type=EmailToMember.SEND_TYPE_WELCOME) | Q(email_type=EmailToMember.SEND_TYPE_DATA_RECORD),
        member=member
    )

    for email in emails_in_queue:
        email.render_subject_and_body()
        email.save()

    if not emails_in_queue:
        EmailToMember(email_type=EmailToMember.SEND_TYPE_DATA_RECORD, member=member).save()


def send_mail_on_member_modification(sender, instance, created, **kwargs):
    """All logic for sending emails on modifications of a member in one place. Using a signal makes sure that no matter
    which part of the object is modified and from which part inside the code the modification is done, this will always
    be called."""
    # If a new Member is created create a welcome email
    # If a Member or EmailAddress is modified or an EmailAddress created, then check if either a welcome or data record
    # email exist. If none exists create a data record email and otherwise re-render subject and body to reflect the
    # most recently stored data.
    # Only do anything if any field of interest has been changed.
    if getattr(instance, '_imported', False):
        # No emails for imported Members/EmailAddresses
        return
    elif sender.__name__ == 'Member':
        member = instance
        if created:
            if not member.is_active or member.erfa.has_doppelmitgliedschaft:
                return
            EmailToMember(email_type=EmailToMember.SEND_TYPE_WELCOME, member=member).save()
            return
        elif not member.have_fields_changed(
            ['first_name', 'last_name', 'address_1', 'address_2', 'address_3', 'address_country', 'address_unknown',
             'comment', 'erfa', 'fee_last_paid', 'fee_override', 'fee_paid_until', 'account_balance', 'membership_end',
             'membership_reduced', 'membership_type']
        ):
            return
    elif sender.__name__ == 'EmailAddress':
        if instance.have_fields_changed(['email_address', 'gpg_key_id']):
            member = instance.member
        else:
            return
    else:
        return

    # For a good measure check if a payment is due every time a member is modified
    # This will call save() again, causing a short loop until the paid until date is in the future
    # ToDo: think about how this is done best
    # member.execute_payment_if_due()

    if not member.is_active or member.erfa.has_doppelmitgliedschaft:
        return

    send_or_refresh_data_record(member)


post_save.connect(send_mail_on_member_modification)
