# -*- coding: utf-8 -*-

import datetime
import json

import gpg
import pendulum
import re
from collections import OrderedDict
from django.core import exceptions
from django.core.mail import EmailMessage
from django.db.models import Count, Q
from django.http import JsonResponse
from django.shortcuts import HttpResponse, render
from django.template.loader import get_template, select_template

import ROOT.helper as helper
from ROOT.helper import i_request, str2bool
from ROOT.settings import (
    EMAIL_HOST_USER, EMAIL_MANAGING_DIRECTOR, FEE_NOTIFICATION_TIME,
    GPG_HOST_USER, GPG_MANAGING_DIRECTOR,
)
from members.countryfield import COUNTRIES
from members.models import (
    EmailAddress, EmailToMember, Erfa, Member,
    send_or_refresh_data_record,
)

RETURN_TYPE_JSON = 'json'
RETURN_TYPE_HTML = 'html'

MAIL_CREATE_NEW_USER = 'new_user'
MAIL_CREATE_DELAYED_PAYMENT = 'delayed_payment'


def _get_return_type(request):
    return_as = RETURN_TYPE_JSON
    if 'return_as' in i_request(request):
        return_as = i_request(request)['return_as']
    return return_as


def index(request):
    return render(request, 'api/index_api.html', {})


def country_analysis(request):
    return_as = _get_return_type(request)
    country_dict = {}

    for member in Member.objects.members_only():
        if member.address_country not in country_dict:

            country_name = ''
            for countryPair in COUNTRIES:
                if countryPair[0] == member.address_country:
                    country_name = countryPair[1]
            country_dict[member.address_country] = {
                'country': member.address_country,
                'country_name': country_name,
                'members': 0
            }
        country_dict[member.address_country]['members'] += 1

    if return_as == RETURN_TYPE_JSON:
        json_list = []
        for countryKey in sorted(country_dict):
            json_list.append([
                countryKey, country_dict[countryKey]['country_name'],
                country_dict[countryKey]['members']
            ])

        return HttpResponse(json.dumps(json_list), content_type='application/json')

    return HttpResponse('BAD FORMAT', content_type='text/html')


def zip_analysis(request):
    return_as = _get_return_type(request)

    country = 'DE'
    if 'country' in i_request(request):
        country = i_request(request)['country']

    member_list = Member.objects.filter(address_country=country)

    zip_member_count_dict = {}
    zip_member_count_total = 0
    zip_count_total = 0

    for member in member_list:
        if member.is_member():
            plz = member.get_plz()
            if plz != '':
                plz = plz[:2]
                if plz not in zip_member_count_dict:
                    zip_member_count_dict[plz] = 0
                    zip_count_total += 1
                zip_member_count_dict[plz] += 1
                zip_member_count_total += 1

    zip_member_count_list = []
    for item in zip_member_count_dict.items():
        zip_member_count_list.append(item)

    if return_as == RETURN_TYPE_JSON:
        return HttpResponse(json.dumps({'zip_member_count_list': zip_member_count_list,
                                        'zip_member_count_total': zip_member_count_total,
                                        'zip_count_total': zip_count_total}), content_type='application/json')

    elif return_as == RETURN_TYPE_HTML:
        parsed_template = get_template('api_response_table_simple.html').render({'dataList': zip_member_count_list})

        return HttpResponse('<p>Total Members: ' + str(zip_member_count_total) + '<br />Total ZIP Codes:'
                            + str(zip_count_total) + '</p>' + parsed_template, content_type='text/html')

    return HttpResponse('BAD FORMAT', content_type='text/html')


def member_info(request):
    """
    get infos about a member by chaos_number

    chaos_number
    separator

    :param request:
    :return:
    """

    separator = '<br>'

    if 'separator' in i_request(request):  # request.REQUEST:
        separator = i_request(request)['separator']  # request.REQUEST['separator']
    if separator == '\\n':
        separator = '\n'

    all_infos = ''
    if 'chaos_number' in i_request(request):
        member = Member.objects.get(chaos_number=int(i_request(request)['chaos_number']))
        all_infos += str(member) + separator
        all_infos += 'Name: ' + member.get_name() + separator
        all_infos += 'Address: ' + member.get_address() + separator
        all_infos += 'Country: ' + member.address_country + separator
        all_infos += 'PLZ: ' + member.get_plz() + separator
        if member.email_is_unknown():
            all_infos += 'email is unknown' + separator
        else:
            all_infos += separator.join(str(v) for v in member.get_emails())

    if all_infos == '':
        all_infos = 'nothing to do ' + ', '.join(i_request(request))

    return HttpResponse(all_infos, content_type='text')


def member_queue_data_record_email(request):
    """
    Send a data record email to the specified Member(s) no matter if anything in their data set changed. Will refresh
     already queued emails instead of queueing multiple emails.
    :param request: Should be a GET request with one or more parameters 'chaos_number', being the chaos_number of a
    Member
    :return: A JSON string with a dictionary of each chaos number paired with either 'queued' or 'not found'
    """
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number)
            send_or_refresh_data_record(member)
            response[chaos_number] = 'queued'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found'
    return HttpResponse(json.dumps(response), content_type='application/json')


def member_inactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=True)
            member.is_active = False
            member.save()
            response[chaos_number] = 'inactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already inactive'
    return HttpResponse(json.dumps(response), content_type='application/json')


def member_reactivate(request):
    response = {}
    for chaos_number in i_request(request).getlist('chaos_number'):
        try:
            member = Member.objects.members_only().get(chaos_number=chaos_number, is_active=False)
            member.is_active = True
            member.save()
            response[chaos_number] = 'reactivated'
        except Member.DoesNotExist:
            response[chaos_number] = 'not found or already active'
    return HttpResponse(json.dumps(response), content_type='application/json')


def billing_cycle(request):
    """
    :param request:
    :return: HttpResponse (json)
    """
    del request

    booked = map(lambda member: {'member': str(member),
                                 'memberName': member.get_name(),
                                 'balance': member.account_balance} if member.execute_payment_if_due() else None,
                 Member.objects.all())
    booked = list(filter(lambda x: x is not None, booked))

    return HttpResponse(json.dumps(booked), content_type='application/json')


def _mail_create_by_id(mail_type, u_id):
    return _mail_create(mail_type, u_id)


def mail_create(request):
    mail_type = ''
    user_id = -1
    if MAIL_CREATE_NEW_USER in i_request(request):
        mail_type = MAIL_CREATE_NEW_USER
        user_id = i_request(request)[MAIL_CREATE_NEW_USER]
    elif MAIL_CREATE_DELAYED_PAYMENT in i_request(request):
        mail_type = MAIL_CREATE_DELAYED_PAYMENT

    response = _mail_create(mail_type, user_id)

    return HttpResponse(json.dumps(response), content_type='application/json')


def _get_delayed_payment_members():
    """Finds all members who are overdue with their payments. Members are only included if the payment was due more than
    FEE_NOTIFICATION_TIME (set in settings.py) months ago and the member is active and not a Doppelmitglied in any Erfa.
    This method will return an empty queryset, if FEE_NOTIFICATION_TIME is not a valid integer.

    :return: A queryset holding all overdue members.
    """
    if not isinstance(FEE_NOTIFICATION_TIME, int):
        print("Config value FEE_NOTIFICATION_TIME has to be an integer representing months.")
        return Member.objects.none()

    # To calculate if a member is overdue a date to compare against the paid_until_date is calculated like this:
    # today + 1 year - FEE_NOTIFICATION_TIME months
    # If the paid_until_date is less than this cut off date, then the payment is overdue.

    cut_off_date = pendulum.datetime.today().add(years=1).subtract(months=FEE_NOTIFICATION_TIME).date()

    return Member.objects.filter(
        fee_paid_until__lte=cut_off_date,
        account_balance__lt=0,
        is_active=True,
        erfa__has_doppelmitgliedschaft=False
    )


def _mail_create(mail_type, user_id):
    response = []
    member_list = []
    context_dict = {}

    mails_skipped = 0
    mails_errors = 0
    mails_added = 0

    if MAIL_CREATE_DELAYED_PAYMENT == mail_type:

        member_list = _get_delayed_payment_members()
        subject = 'Zahlungsverzug'  # TODO: Translate!
        template_prefix = 'mail_templates/mail_delayedpayment_'
        mail_to_send_type = EmailToMember.SEND_TYPE_DELAYED_PAYMENT
        # +2 month AND not empty
    else:
        return {'state': 'error'}

    members_i_cannot_mail = []
    for member in member_list:
        if not member.is_active:  # yeah, looks bad, waiting for an better idea to do that -.-
            continue
        context_dict['member'] = member

        # TODO: complete the calculation!
        context_dict['toPay'] = '{0:.2f}'.format(member.get_money_to_pay() / 100)

        template = select_template([template_prefix + member.address_country + '.txt.html',
                                    template_prefix + 'EN.txt.html'])
        parsed_template = template.render(context_dict)

        try:
            mail = member.get_primary_mail()
            if not mail:
                # member has no mail address, i don't care!
                mails_skipped += 1
                members_i_cannot_mail.append('has no Email Address: ' + str(member))
                continue
            msg = EmailToMember()

            msg.member = member
            msg.body = parsed_template
            msg.subject = subject
            msg.created = datetime.datetime.now()
            msg.email_type = mail_to_send_type

            try:
                # no problem with multiple existing mailadresses, because foreign key=member/chaosnr
                msg_exists = EmailToMember.objects.filter(member=member, subject=subject)
                if msg_exists.count() > 0:
                    mails_skipped += 1
                    members_i_cannot_mail.append('SKIPPED: Mail with subject "' + subject +
                                                 '" already exists in queue for: ' + str(member))
                else:
                    mails_added += 1
                    msg.save()
                    response.append(str(mail.email_address))

            except Exception as ex:
                mails_errors += 1
                print(ex)
                members_i_cannot_mail.append('ERROR: checkExistingMails ' + str(member) + ' Ex: ' + str(ex))
        except exceptions.ObjectDoesNotExist:
            # member has no mail address, i don't care!
            members_i_cannot_mail.append('has no Email Address: ' + str(member))
            mails_skipped += 1
        except Exception as ex:
            mails_errors += 1
            print(ex)
            members_i_cannot_mail.append('ERROR: ' + str(member) + ' Ex: ' + str(ex))

    return ['mails_added: ' + str(mails_added), 'mails_skipped: ' + str(mails_skipped), 'mails_errors: ' +
            str(mails_errors)] + members_i_cannot_mail + response


def mail_send_next(request):
    mail_to_send = EmailToMember.objects.first()
    if mail_to_send:
        response = mail_to_send.send()
    else:
        response = {'state': 'nothing to send'}
    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_send_all(request):
    response = [mail.send() for mail in EmailToMember.objects.all()]

    if len(response) == 0:
        response = {'state': 'nothing to send'}

    return HttpResponse(json.dumps(response), content_type='application/json')


def mail_still_to_send(request):
    number_of_mails = EmailToMember.objects.all().count()
    return HttpResponse(json.dumps({'numberOfMailsToSend': number_of_mails}), content_type='application/json')


def search_member_db(request):
    """Handles member DB search requests returning HTML-table-rows"""
    context = {}
    if request.method == 'POST':
        params = request.POST
        check_empty_first_name = params.get('check_empty_first_name')
        check_empty_last_name = params.get('check_empty_last_name')
        check_empty_address = params.get('check_empty_address')
        check_empty_country = params.get('check_empty_country')
        check_empty_email = params.get('check_empty_email_address')
        if 'true' in (check_empty_address, check_empty_country, check_empty_first_name, check_empty_last_name,
                      check_empty_email):
            # Search for empty fields
            q = Member.objects.members_only()
            if check_empty_first_name == 'true':
                q = q.filter(Q(first_name__exact='') | Q(first_name__isnull=True))
                print(q)
            if check_empty_last_name == 'true':
                q = q.filter(Q(last_name__exact='') | Q(last_name__isnull=True))
            if check_empty_address == 'true':
                # Filter for datasets with all three address field empty
                q = q.filter(
                    (
                        (Q(address_1__exact='') |
                         Q(address_1__isnull=True)) &
                        (Q(address_2__exact='') |
                         Q(address_2__isnull=True)) &
                        (Q(address_3__exact='') |
                         Q(address_3__isnull=True))
                    ) | Q(address_unknown=True)
                )
            if check_empty_country == 'true':
                q = q.filter(Q(address_country__exact='') | Q(address_country__isnull=True))
            if check_empty_email == 'true':
                members = EmailAddress.objects.all().values('member')
                m = Member.objects.all()
                for member in members:
                    m = m.exclude(chaos_number__exact=member['member'])
                q = q & m

            context['results'] = q
        else:
            first_name = params.get('first_name', '')
            last_name = params.get('last_name', '')
            chaos_number = params.get('chaos_id', '')
            address = params.get('address', '')
            email = params.get('email_address', '')
            fee_reduced = str2bool(params.get('fee_is_reduced', 'false'))
            is_active = str2bool(params.get('is_active', 'false'))
            apply_filters = str2bool(params.get('apply_filters', 'false'))
            q = Member.objects.members_only()
            if first_name != '':
                q = q.filter(first_name__icontains=first_name)
            if last_name != '':
                q = q.filter(last_name__icontains=last_name)
            if chaos_number != '':  # maybe check for illegal characters in the future
                q = q.filter(chaos_number=chaos_number)
            if address != '':
                q = q.filter(Q(address_1__icontains=address) | Q(address_2__icontains=address)
                             | Q(address_3__icontains=address))
            if email != '':
                q = q.filter(Q(emailaddress__email_address__icontains=email))
            if apply_filters:
                q = q.filter(membership_reduced=fee_reduced, is_active=is_active)

            context['results'] = q
        return render(request, 'api/member_search_result.html', context)
    return HttpResponse('Neither Get nor Post')  # HttpResponse('BAD FORMAT', content_type='text/html')


def _erfa_statistics():
    erfa_statistics = OrderedDict()
    members = Member.objects.members_only()
    counts = members.all().annotate(number_members=Count('erfa'))
    counts = counts.values('membership_type', 'erfa').annotate(num_type_members=Count('erfa'))
    all_erfas = Erfa.objects.order_by('long_name', '-has_doppelmitgliedschaft')

    for erfa in all_erfas:
        # Erfa-Kreise sind Erfas ohne Doppelmitgliedschaft und Vereine sind Erfa-Kreise mit Doppelmitgliedschaft.
        erfa_prefix = 'Verein' if erfa.has_doppelmitgliedschaft else 'Erfa-Kreis'
        erfa_statistics[str(erfa)] = {
            'erfa_prefix': erfa_prefix,
            'monthly_income_total': 0,
            'monthly_income_full': 0,
            'monthly_income_reduced': 0,
            'monthly_income_special': 0,
            Member.MEMBERSHIP_TYPE_SUPPORTER: 0,
            Member.MEMBERSHIP_TYPE_MEMBER: 0,
            Member.MEMBERSHIP_TYPE_HONORARY: 0,
            'payer_count_full': 0,
            'payer_count_reduced': 0,
            'payer_count_special': 0
        }

    for c in counts:
        erfa_id = c['erfa']

        erfa_name = str(Erfa.objects.get(pk=erfa_id))  # slow due to nesting, but n<1000
        member_type = c['membership_type']
        number = c['num_type_members']
        erfa_statistics[erfa_name].update({member_type: number})
        members_of_erfa = members.filter(erfa=erfa_id)
        print(erfa_name)
        for erfa_member in members_of_erfa:

            member_pays = helper.get_fee_monthly(erfa_member.get_annual_fee()) / 100.0
            print(erfa_member, erfa_member.get_annual_fee(), member_pays)
            erfa_statistics[erfa_name]['monthly_income_total'] += member_pays
            if erfa_member.membership_reduced:
                erfa_statistics[erfa_name]['payer_count_reduced'] += 1
                erfa_statistics[erfa_name]['monthly_income_reduced'] += member_pays
            elif erfa_member.fee_override is not None:
                erfa_statistics[erfa_name]['payer_count_special'] += 1
                erfa_statistics[erfa_name]['monthly_income_special'] += member_pays
            else:
                erfa_statistics[erfa_name]['payer_count_full'] += 1
                erfa_statistics[erfa_name]['monthly_income_full'] += member_pays

    return erfa_statistics


def get_erfa_statistics(request):

    def get_all_annual_fees_from_list(member_list):
        return sum(map(Member.get_annual_fee, member_list))

    statistic = _erfa_statistics()
    print(statistic)
    if EMAIL_MANAGING_DIRECTOR != '':

        # context dict for template parsing
        # we need an EMPTY string to rjust/ljust it in the template
        context_dict = {'emptystring': '', 'statistic': statistic,
                        'statistics_created_for': datetime.date.today().strftime('%B %Y'),
                        'longest_erfa_text': max(len(k) for k, v in statistic.items() if len(v) != 0)}

        # get the overview
        paying_members = Member.objects.members_only().filter(
            membership_type=Member.MEMBERSHIP_TYPE_MEMBER,
            is_active=True
        )

        paying_supporters = Member.objects.members_only().filter(
            membership_type=Member.MEMBERSHIP_TYPE_SUPPORTER,
            is_active=True
        )

        members_full = paying_members.filter(
            membership_reduced=False,
            fee_override__isnull=True
        )
        members_reduced = paying_members.filter(
            membership_reduced=True,
            fee_override__isnull=True
        )
        members_fee_override = paying_members.filter(
            fee_override__isnull=False
        )
        supporters_full = paying_supporters.filter(
            membership_reduced=False,
            fee_override__isnull=True
        )
        supporters_reduced = paying_supporters.filter(
            membership_reduced=True,
            fee_override__isnull=True
        )
        supporters_fee_override = paying_supporters.filter(
            fee_override__isnull=False
        )
        members_inactive = Member.objects.members_only().filter(
            is_active=False
        )
        members_honorary = Member.objects.members_only().filter(
            membership_type=Member.MEMBERSHIP_TYPE_HONORARY
        )

        context_dict['count_members_full'] = members_full.count()
        context_dict['count_members_reduced'] = members_reduced.count()
        context_dict['count_members_fee_override'] = members_fee_override.count()
        context_dict['count_supporters_full'] = supporters_full.count()
        context_dict['count_supporters_reduced'] = supporters_reduced.count()
        context_dict['count_supporters_fee_override'] = supporters_fee_override.count()
        context_dict['count_members_inactive'] = members_inactive.count()
        context_dict['count_members_honorary'] = members_honorary.count()
        context_dict['count_members_total'] = Member.objects.members_only().count()

        if not (context_dict['count_members_full'] + context_dict['count_members_reduced'] +
                context_dict['count_members_fee_override'] + context_dict['count_members_inactive'] +
                context_dict['count_members_honorary'] + context_dict['count_supporters_full'] +
                context_dict['count_supporters_reduced'] + context_dict['count_supporters_fee_override']
                == context_dict['count_members_total']):
            raise AssertionError("The sum of the different member types and states does not equal the total number of"
                                 " members")

        income_members_full_cent = get_all_annual_fees_from_list(members_full)
        income_members_reduced_cent = get_all_annual_fees_from_list(members_reduced)
        income_members_fee_override_cent = get_all_annual_fees_from_list(members_fee_override)
        context_dict['income_members_full'] = '{:0.2f}'.format(float(income_members_full_cent) / 100.0)
        context_dict['income_members_reduced'] = '{:0.2f}'.format(float(income_members_reduced_cent) / 100.0)
        context_dict['income_members_fee_override'] =\
            '{:0.2f}'.format(float(income_members_fee_override_cent) / 100.0)
        income_yearly_cent = income_members_full_cent + income_members_reduced_cent\
            + income_members_fee_override_cent
        context_dict['income_yearly'] = '{:0.2f}'.format(float(income_yearly_cent) / 100.0)
        context_dict['income_monthly'] = '{:0.2f}'.format(float(income_yearly_cent / 12) / 100.0)

        delayed_payment_members = _get_delayed_payment_members()

        context_dict['delayed_payment_notification_time'] = FEE_NOTIFICATION_TIME
        context_dict['delayed_payment_members'] = delayed_payment_members.filter(
            membership_reduced=False,
            fee_override__isnull=True
        ).count()
        context_dict['delayed_payment_members_reduced'] = delayed_payment_members.filter(
            membership_reduced=True,
            fee_override__isnull=True
        ).count()
        context_dict['delayed_payment_members_fee_override'] = delayed_payment_members.filter(
            fee_override__isnull=False
        ).count()

#             - Mitgliedern .........: {{ income_members_full|rjust:7 }} EUR
#             - erm. Mitgliedern.....: {{ income_members_reduced|rjust:7 }} EUR
#             - erm. Mitglieder Spez.: {{ income_members_fee_override|rjust:7 }} EUR
#                          ===========
#                   Total: {{ income_yearly }} EUR  Dies entspricht {{ income_monthly }} EUR im Monat.
#
#                   - Mitglieder ..........: {{ delayed_payment_members|rjust:5 }}
# - erm. Mitglieder......: {{ delayed_payment_members_reduced|rjust:5 }}
# - erm. Mitglieder Spez.: {{ delayed_payment_members_fee_override|rjust:5 }}
#

        template = select_template(['mail_templates/mail_erfa_statistic.html', ])
        parsed_template = template.render(context_dict)

        with gpg.Context(armor=True) as c:
            sender_key = c.get_key(GPG_HOST_USER)
            c.signers.append(sender_key)

            recipient_key = c.get_key(GPG_MANAGING_DIRECTOR)
            encrypted_body, _, _ = c.encrypt(parsed_template.encode(), recipients=[sender_key, recipient_key],
                                             always_trust=True, sign=True)

            send_email = EmailMessage(
                subject='Monatliche Statistik',
                body=encrypted_body.decode('utf-8'),
                from_email=EMAIL_HOST_USER,
                to=[EMAIL_MANAGING_DIRECTOR, ],
                bcc=[EMAIL_HOST_USER, ]
            )
            send_email.send(False)

            return HttpResponse('<pre>' + parsed_template + '</pre>')

    return JsonResponse(statistic)


def create_delayed_payment_export():
    member_list = _get_delayed_payment_members()
    # final_file_string = ''
    import csv

    with open('/tmp/delayed_payment_export.csv', 'w', newline='\n') as csvfile:
        export_writer = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        export_writer.writerow(['chaos_number',
                                'first_name',
                                'last_name',
                                'address_1',
                                'address_2',
                                'address_3',
                                'address_country',
                                'state',
                                'entry_date',
                                'yearly_fee',
                                'last_payment',
                                'paid_until',
                                'balance',
                                'open_years',
                                'fee_to_pay'])
        for member in member_list:
            row_arr = [member.chaos_number, member.first_name, member.last_name, member.address_1, member.address_2,
                       member.address_3, member.address_country, 'aktiv', member.membership_start,
                       member.get_annual_fee(), member.fee_last_paid, member.fee_paid_until,
                       member.get_balance_readable(), 7, member.get_money_to_pay()]
            export_writer.writerow(row_arr)


def clear_members(request):
    _request = i_request(request)

    response = {}
    if 'deleteMember' in _request:
        response = _member_exit(_request['deleteMember'])
    elif 'deleteMembers' in _request:
        for memberId in _request['deleteMembers']:
            response['memberId'] = _member_exit(memberId)

    return JsonResponse(response)


def _member_exit(chaos_number):
    error_message = ''
    try:
        m = Member.objects.get(chaos_number__exact=chaos_number)
        m.exit()
        was_ok = True
    except Exception as ex:
        was_ok = False
        error_message = str(ex)

    return {'error_message': error_message, 'was_ok': was_ok}


def drop_transactions(request):
    # http://localhost:8000/api/drop_transactions?reallydroptransactions=yes
    _request = i_request(request)
    response = {'errorMessage': '', 'wasCleared': False, 'transactionsDropped': 0}
    if 'reallydroptransactions' in _request and _request['reallydroptransactions'] == 'yes':
        try:
            from import_app.models import Transaction
            all_transactions = Transaction.objects.all()
            count_transactions = all_transactions.count()
            all_transactions.delete()
            response['transactionsDropped'] = count_transactions
            response['wasCleared'] = True
        except Exception as ex:
            response['errorMessage'] = str(ex)

    return JsonResponse(response)


def bulk_address_unknown(request):
    changes = {'success': 0, 'error': 0, 'no_data': False}
    if request.method == 'POST':
        chaos_numbers = request.POST['chaos_numbers'].split('\n')
        for chaos_number in chaos_numbers:
            nr = re.search('([0-9]+)', chaos_number).group(1)
            print(nr)
            m = Member.objects.get(chaos_number__exact=nr)
            print(m)
            try:
                m.address_unknown = True
                m.save()
                changes['success'] += 1
            except:
                changes['error'] += 1
    else:
        changes['no_data'] = True
    return JsonResponse(changes)
